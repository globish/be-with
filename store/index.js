import Vuex from 'vuex'
import Axios from 'axios';

const createStore = () => {
  return new Vuex.Store({
    state: {
      message: {
        show: false,
        text: ""
      },
      color: null,
      counter: 0,
      animals: [{
          id: 1,
          name: 'Penguin',
          svg: require('~/assets/svg/penguin.svg'),
          level: 2,
          colors: ['red','green','blue','yellow','black','white','grey','pink','orange','brown','purple','violet','gold']
        },
        {
          id: 2,
          name: 'Bugs',
          svg: require('~/assets/svg/bugs.svg'),
          level: 3,
          colors: ['red','green','blue','yellow','black','white','grey','pink','orange','brown','purple','violet','gold']
        },
        {
          id: 3,
          name: 'Flower',
          svg: require('~/assets/svg/flower.svg'),
          level: 1,
          colors: ['red','green','blue','yellow','black','white','grey','pink','orange','brown','purple','violet','gold']
        },
        {
          id: 4,
          name: 'Flower',
          svg: require('~/assets/svg/flower2.svg'),
          level: 1,
          colors: ['red','green','blue','yellow','black','white','grey','pink','orange','brown','purple','violet','gold']
        }
      ]
    },
    mutations: {
      setMessage(state, payload) {
        state.message = payload
      },
      setColor(state, color) {
        state.color = color;
        state.counter++;
      },
    },
    actions: {
      async speech2Text({
        commit
      }, formData) {
        const {
          data
        } = await Axios({
          method: 'post',
          url: 'https://kriang.space/speech/color',
          data: formData,
          config: {
            headers: {
              'Content-Type': 'multipart/form-data',
              'Access-Control-Allow-Origin': true
            },
            credentials: true
          }
        });
        commit('setColor', data.data.speech.color);
      }
    }
  })
}

export default createStore
